import { useEffect } from 'react';
import { connect } from 'react-redux';
import { fetchWidgets } from './actions/widgets';

import { Card, CardHeader } from '@material-ui/core';
import styled from 'styled-components';

import ErrorMessage from './components/ErrorMessage';
import WidgetList from './components/WidgetList';

const Content = styled.div`
  width: 600px;
  margin: 60px auto;
`;

function WidgetsListPage({loadWidgets, state}) {
    useEffect(() => { 
        loadWidgets();
    }, []);
    debugger;
    return <Content>
    {state.status === "LOADING_WIDGETS" && <>Loading...</>}
    {state.status === "SUCCESS" && <Card>
        <CardHeader title="All Widgets" />
        <WidgetList widgets={state.widgets} />
    </Card>}
    {state.status === "FAILURE" && <ErrorMessage onClick={loadWidgets} error={state.error} />}
</Content>;
}
const mapDispatchToProps = dispatch => {
    return {
      loadWidgets: () => {
        dispatch(fetchWidgets());
      }
    };
  };

const mapStateToProps = state => {
    return { state };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WidgetsListPage);