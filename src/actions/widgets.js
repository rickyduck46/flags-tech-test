export const FETCH_WIDGETS_STARTED = 'FETCH_WIDGETS_STARTED';
export const FETCH_WIDGETS_SUCCESS = 'FETCH_WIDGETS_SUCCESS';
export const FETCH_WIDGETS_FAILURE = 'FETCH_WIDGETS_FAILURE';
export const FETCH_WIDGET_STARTED = 'FETCH_WIDGET_STARTED';
export const FETCH_WIDGET_SUCCESS = 'FETCH_WIDGET_SUCCESS';
export const FETCH_WIDGET_FAILURE = 'FETCH_WIDGET_FAILURE';

export const fetchWidgets = () => {
    return dispatch => {
      dispatch(fetchWidgetsStarted());
  
      fetch(`/api/widgets`)
        .then(res => res.json() )
        .then(json => {
            dispatch(fetchWidgetsSuccess(json));
        })
        .catch(err => {
          dispatch(fetchWidgetsFailure(err.message));
        });
    };
  };

  const fetchWidgetsSuccess = widgets => ({
    type: FETCH_WIDGETS_SUCCESS,
    payload: {
      widgets
    }
  });
  
  const fetchWidgetsStarted = () => ({
    type: FETCH_WIDGETS_STARTED
  });
  
  const fetchWidgetsFailure = error => ({
    type: FETCH_WIDGETS_STARTED,
    payload: {
      error
    }
  });
  
//export const fetchWidget = (id) => ({type: FETCH_WIDGET, id});
export const fetchWidget = (id) => {
  debugger;
  return dispatch => {
    dispatch(fetchWidgetStarted());

    fetch(`/api/widgets/${id}`)
      .then(res => res.json() )
      .then(json => {
        debugger;
          dispatch(fetchWidgetSuccess(json));
      })
      .catch(err => {
        dispatch(fetchWidgetFailure(err.message));
      });
  };
};

const fetchWidgetSuccess = widget => ({
  type: FETCH_WIDGET_SUCCESS,
  payload: {
    widget
  }
});

const fetchWidgetStarted = () => ({
  type: FETCH_WIDGET_STARTED
});

const fetchWidgetFailure = error => ({
  type: FETCH_WIDGET_STARTED,
  payload: {
    error
  }
});
