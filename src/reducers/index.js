export default function widgets(state = { widgets: [], status: "INITIALISED", error: null, widget: null}, action) {
    switch (action.type) {
      case 'FETCH_WIDGETS_STARTED':        
        return {...state, status: "LOADING_WIDGETS", error: null}
      case 'FETCH_WIDGETS_SUCCESS':
        return {...state, widgets: action.payload.widgets, status: "SUCCESS"}
      case 'FETCH_WIDGETS_FAILED':
        return {...state, status: "FAILURE", error: action.error}
      case 'FETCH_WIDGET_STARTED':
        return {...state, status: "LOADING_WIDGET", error: null}
      case 'FETCH_WIDGET_SUCCESS':
        return {...state, widget: action.payload.widget, status: "WIDGET_SUCCESS"}
      case 'FETCH_WIDGET_FAILED':
        return {...state, status: "WIDGET_FAILURE", error: action.payload.error}
      default:
        return state
    }
  }