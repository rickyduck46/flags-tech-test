
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


  
  export default function WidgetDetails({widget}) {
  
    return (
      <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Property</TableCell>
              <TableCell>Value</TableCell>
             
            </TableRow>
          </TableHead>
          <TableBody>
              <TableRow key="id">
                <TableCell component="th" scope="row">
                  ID
                </TableCell>
                <TableCell>{widget.id}</TableCell>  
              </TableRow>
              <TableRow key="min">
                <TableCell component="th" scope="row">
                  Min
                </TableCell>
                <TableCell>{widget.min}</TableCell>  
              </TableRow>
              <TableRow key="max">
                <TableCell component="th" scope="row">
                  Max
                </TableCell>
                <TableCell>{widget.max}</TableCell>  
              </TableRow>
              <TableRow key="current">
                <TableCell component="th" scope="row">
                  Current
                </TableCell>
                <TableCell>{widget.current}</TableCell>  
              </TableRow>
              <TableRow key="id">
                <TableCell component="th" scope="row">
                  History
                </TableCell>
                <TableCell>{widget.history ? widget.history.toString() : "No history to show"}</TableCell>  
              </TableRow>

          </TableBody>
        </Table>
      </TableContainer>
    );
  }