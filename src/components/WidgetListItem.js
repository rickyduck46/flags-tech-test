import { ListItem } from '@material-ui/core';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const WidgetItem = styled(ListItem)`
    border-bottom: solid 1px #CCC;
    padding: 0;
    margin: 0;
    width: 100%;
    cursor: pointer;
    &:hover {
        background: #CCC;
    }
   
    & img {
        margin-right: 20px;
    }
    &:last-child {
        border-bottom: none;
    }
`;

const WidgetLink = styled(Link)`
    width: 100%;
    color: #333;
    padding: 20px 0;
    margin: 0;
    &:focus {
        background: #EEE;
        outline: none;
    }
`;

function WidgetListItem({widget}) {
    return <WidgetItem key={widget.id}>
        <WidgetLink to={`/widget/${widget.id}`}>
            <img src={widget.image} width="26" alt={widget.name} />
            {widget.name}
        </WidgetLink>
    </WidgetItem>
}

export default WidgetListItem;