import { Button } from '@material-ui/core';

const ErrorMessage = ({ onClick, error}) => <>There was an error: {error}. <Button onClick={onClick}>Try again</Button></>;

export default ErrorMessage;