import { List } from '@material-ui/core';

import WidgetListItem from './WidgetListItem';

function WidgetList({widgets}) {
    return <List>
    {widgets.map((widget) => <WidgetListItem key={widget.id} widget={widget} />)}
    </List>
}

export default WidgetList;