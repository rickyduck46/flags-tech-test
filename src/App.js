import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import WidgetsListPage from './WidgetsListPage';
import WidgetPage from './WidgetPage';

function App({onLoad, fetchWidget, state}) {
  return (
    <Router>
      <Switch>
      <Route path="/widget/:id">
          <WidgetPage fetchWidget={fetchWidget} />
        </Route>
        <Route path="/">
          <WidgetsListPage loadWidgets={onLoad} state={state} />
        </Route>
        
      </Switch>
    </Router>
  );
}

export default App;
