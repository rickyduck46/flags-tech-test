import { useEffect } from 'react';
import { useParams } from 'react-router-dom';

import { Card, CardHeader, Button } from '@material-ui/core';
import styled from 'styled-components';

import { Link } from 'react-router-dom';

import { connect } from 'react-redux';

import { fetchWidget } from './actions/widgets';
import ErrorMessage from './components/ErrorMessage';
import WidgetDetails from './components/WidgetDetails';

const Content = styled.div`
  width: 600px;
  margin: 60px auto;
`;

const WidgetImage = styled.img`
  width: 100px;
  margin: 20px;
`

const HomeButton = styled(Button)`
  float:right;
  margin-right: 20px;
`

function WidgetPage({fetchWidget, state}) {
    const { id } = useParams();
    useEffect(() => { 
      fetchWidget(id); 
    }, [fetchWidget, id]);
    const { widget, status, error } = state;
    return <Content>
          {status === "LOADING_WIDGET" && <>Loading...</>}
          {status === "WIDGET_SUCCESS" && <Card>
            <CardHeader title={widget.name} />
            <Link to="/"><HomeButton>Back To List</HomeButton></Link>
            <WidgetImage src={widget.image} width="150" alt={widget.name} /><br />
            <WidgetDetails widget={widget} />
          </Card>}
          {status === "WIDGET_FAILURE" && <ErrorMessage onClick={fetchWidget(id)} error={error} />}
</Content>;

}

const mapDispatchToProps = dispatch => {
  return {
    fetchWidget: (id) => {
      dispatch(fetchWidget(id));
    }
  };
};

const mapStateToProps = state => {
  return { state };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WidgetPage);